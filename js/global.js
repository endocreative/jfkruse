$(document).ready( function() {

	$('.tab-label').click(function() {
		var tab = $(this).data('src');

		$('.tab-label').removeClass('active');
		$('.tab-content').removeClass('active');

		$(this).addClass('active');
		$(tab).addClass('active');
	});

});